﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Keys : int
{
    heart = 0,
    oxygen,
    eyes,
    bowels,
    sanity,
    hygiene,
    write,
    shower,
    toilet,
    pause
}

public class InputController : MonoBehaviour {
    public static InputController ic;
    
    /*
     * To add new entries, add to (enum) Keys, and assign a non-used KeyCode + fitting description (for display to user)
     *
     */
    public Dictionary<int, KeybindPair> keys = new Dictionary<int, KeybindPair>();

    // Extras
    bool lockCursor = false;

    void Awake() {
        if (!ic) { ic = this; DontDestroyOnLoad(gameObject); } else { DestroyImmediate(this); }


        if (!lockCursor) {
            Cursor.lockState = CursorLockMode.None;
        }
    }

    public struct KeybindPair
    {
        public KeyCode kc;
        public string description;
        public KeybindPair(KeyCode _kc, string _name)
        {
            kc = _kc;
            description = _name;
        }
    }

    /// <summary>
    /// Binds a KeyCode and description to a Keys value in a dictionary.
    /// </summary>
    /// <param name="k">Keys enum</param>
    /// <param name="kc">Unity input KeyCode</param>
    /// <param name="formattedDescription">Description with {0} for format</param>
    public static void AddKeybind(Keys k, KeyCode kc, string formattedDescription)
    {
        ic.keys.Add((int)k, new KeybindPair(kc, string.Format(formattedDescription, kc.ToString().ToUpper())));
    }

    public static void ChangeKeybind(Keys k, KeyCode kc)
    {
        KeyCode oldKc = ic.keys[(int)k].kc;
        string desc = ic.keys[(int)k].description;
        ic.keys.Remove((int)k);
        ic.keys.Add((int)k, new KeybindPair(kc, desc.Replace((char)oldKc, (char)kc)));

    }

    public static bool IsKeyPress(Keys k) {
        return Input.GetKeyDown(ic.keys[(int)k].kc);
    }

    public static bool IsKeyDown(Keys k) {
        return Input.GetKey(ic.keys[(int)k].kc);
    }

    void Update() {
        //if (Input.GetKeyDown(keys["lockMouse"].kc)) {
        //    lockCursor = !lockCursor;
        //    UpdateCursor();
        //}

        //if (Input.GetKeyDown(keys["menu"].kc)) {
        //    if (MenuController.mc.menusOrderOpened.Count == 0)
        //    {
        //        MenuController.mc.menus.Find(m => m.MenuName == "OptionsMenu").EnableDisable();
        //    }
        //    else
        //    {
        //        MenuController.mc.menusOrderOpened[0].EnableDisable();
        //    }
        //}

        //if (Input.GetKeyDown(keys["cameraMode"].kc)) {
        //    Camera.main.GetComponent<ThirdPersonCamera>().firstPerson = !Camera.main.GetComponent<ThirdPersonCamera>().firstPerson;
        //}
    }

    void OnApplicationFocus(bool hasFocus) {
        UpdateCursor();
    }

    void UpdateCursor() {
        Cursor.lockState = lockCursor ? CursorLockMode.Locked : CursorLockMode.Confined;
        Cursor.visible = !lockCursor;
    }
}
