﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputTest : MonoBehaviour {
	// Use this for initialization
	void Start ()
    {
        //Example add keybind (Press P to "eyes" (blink))
        InputController.AddKeybind(Keys.eyes, KeyCode.P, "Press {0} to blink!");

        InputController.AddKeybind(Keys.heart, KeyCode.H, "Press {0} to blink!");
        //Example change keybind to functional key in InputController (KeyCode.B (B) now points to Keys.heart)
        InputController.ChangeKeybind(Keys.heart, KeyCode.B);
	}
	
	// Update is called once per frame
	void Update ()
    {
        //Example usage of InputController, check if the "pump blood" key is pressed
        if (InputController.IsKeyPress(Keys.heart))
        {
            print("PUMP THAT SHIT!");
        }
	}
}
