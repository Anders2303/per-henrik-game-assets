﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Needs : MonoBehaviour {
    private List<NeedTimer> needTimers; 
	
    public void AddNewNeed(NeedTimer needTimer)
    {
        needTimers.Add(needTimer);
    }
    
    // Use this for initialization
	void Start () {
        needTimers = new List<NeedTimer>();
        for(int i = 0; i < 5; i++)
        {
            AddNewNeed(new NeedTimer(10, "timer"+i));
        }
	}
	
	// Update is called once per frame
	void Update () {
		foreach(NeedTimer timer in needTimers)
        {
            timer.Update(Time.deltaTime);
        }
	}
}
