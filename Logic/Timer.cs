﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeedTimer {
    private float remainingTime;
    private float timeLossMultiplayer = 1;
    private string name;

    public NeedTimer(float startTime, string name)
    {
        this.remainingTime = startTime;
        this.name = name;
    }

    public void reduceTime(float reductionAmount)
    {
        remainingTime -= reductionAmount;
        checkTime();
    }

    public void reduceTimeByPercent(float percentage)
    {
        remainingTime = (percentage / 100) * remainingTime;
        checkTime();
    }

    public void increaseTime(float reductionAmount)
    {
        remainingTime -= reductionAmount;
    }

    public void increaseTimeByPercent(float percentage)
    {
        remainingTime = (100 / percentage) * remainingTime;
    }

    public void checkTime()
    {
        MonoBehaviour.print(name + remainingTime);
    }

    // Update is called by the Need container

    private bool alreadyPrinted = false;
    public void Update (float deltaTime) {
        if (remainingTime > 0)
        {
            reduceTime(deltaTime);
        } else if (!alreadyPrinted)
        {
            MonoBehaviour.print(name + " ran out of time!");
            alreadyPrinted = true;
        }
	}
}
