﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]private GameObject descBox;
    [SerializeField]private Keys keyToPress;
    private bool hover = false;
    private float timer = 0.0f;
    private float timerTarget = 0.5f;

    void Start()
    {
        descBox.GetComponent<Text>().text = InputController.ic.keys[(int)keyToPress].description;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        hover = true;
        print("Mouse enter");
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        hover = false;
        descBox.SetActive(false);
        timer = 0;
        print("Mouse exit");
    }

	// Update is called once per frame
	void Update ()
    {
        if (hover)
        {
            timer += Time.deltaTime;
            if(timer > timerTarget)
            {
                //TODO: Display description text
                descBox.SetActive(true);
            }
        }
	}
}
